# Script realizado por Guillermo, Ruben, Gabriel y Agustin - DVT - 3

#!/bin/bash

IP_WAN=10.3.4.0/24
IP_DMZ=172.20.124.0/24
IP_LAN=192.168.124.0/24

I_WAN=eth0
I_DMZ=eth1
I_LAN=eth2

IP_WAN_REAL=10.3.4.34

IP_DMZ_REAL_DNS=172.20.124.53
IP_DMZ_REAL_WEB=172.20.124.80

# Reestablecemos el firewall

iptables -F
iptables -t nat -F
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP

# Permitimos el ping

iptables -t filter -A INPUT -p icmp -i $I_WAN -s $IP_WAN -j ACCEPT
iptables -t filter -A INPUT -p icmp -i $I_DMZ -s $IP_DMZ -j ACCEPT
iptables -t filter -A INPUT -p icmp -i $I_LAN -s $IP_LAN -j ACCEPT

iptables -t filter -A OUTPUT -p icmp -o $I_WAN -j ACCEPT	
iptables -t filter -A FORWARD -p icmp -i $I_DMZ -o $I_WAN -j ACCEPT
iptables -t filter -A FORWARD -p icmp -i $I_LAN -o $I_WAN -j ACCEPT

# Permitimos el acceso al SSH desde l DMZ y LAN

iptables -t filter -A INPUT -p tcp -i $I_DMZ -s $IP_DMZ --dport 22 -j ACCEPT

iptables -t filter -A OUTPUT -p tcp --dport 22 -o $I_DMZ -d $IP_DMZ -j ACCEPT

iptables -t filter -A INPUT -p tcp -i $I_LAN -s $IP_LAN --dport 22 -j ACCEPT

iptables -t filter -A OUTPUT -p tcp --dport 22 -o $I_LAN -d $IP_LAN  -j ACCEPT

# Permitimos la conexion al puerto 53 UDP - Entrante/Saliente

# iptables -t filter -A OUTPUT -p udp --dport 53 -o eth0 -j ACCEPT

iptables -t filter -A OUTPUT -p udp --dport 53 -o $I_DMZ -j ACCEPT

iptables -t filter -A OUTPUT -p udp --dport 53 -o $I_LAN -j ACCEPT

iptables -t filter -A FORWARD -p udp --dport 53 -i $I_DMZ -o $I_WAN -j ACCEPT

# Permitimos la conexion al puerto 80 TCP - Entrante/Saliente

# iptables -t filter -A INPUT -p tcp -i eth0 --sport 80 -j ACCEPT

iptables -t filter -A OUTPUT -p tcp -o $I_WAN --dport 80 -j ACCEPT

iptables -t filter -A FORWARD -p tcp --dport 80 -i $I_DMZ -o $I_WAN -j ACCEPT

# Permitimos la conexion a traves del puerto 443

# iptables -t filter -A INPUT -p tcp -i eth0 --sport 443 -j ACCEPT
iptables -t filter -A OUTPUT -p tcp -o $I_WAN --dport 443 -j ACCEPT
iptables -t filter -A FORWARD -p tcp --dport 443 -i $I_DMZ -o $I_WAN -j ACCEPT

# iptables forward

#iptables -t filter -A FORWARD -p tcp --sport 80 -i eth1 -o eth0 -j ACCEPT
#iptables -t filter -A FORWARD -p tcp --sport 443 -i eth1 -o eth0 -j ACCEPT

iptables -t filter -A FORWARD -p tcp --dport 80 -i $I_WAN -o $I_DMZ -d $IP_DMZ_REAL_WEB -j ACCEPT
iptables -t filter -A FORWARD -p tcp --dport 443 -i $I_WAN -o $I_DMZ -d $IP_DMZ_REAL_WEB -j ACCEPT
iptables -t filter -A FORWARD -p udp --dport 53 -i $I_WAN -o $I_DMZ -d $IP_DMZ_REAL_DNS -j ACCEPT
iptables -t filter -A FORWARD -p tcp --dport 53 -i $I_WAN -o $I_DMZ -d $IP_DMZ_REAL_DNS -j ACCEPT

## Nat

#iptables -t nat -A POSTROUTING -s $IP_DMZ -o eth0 -j MASQUERADE

#iptables -t nat -A POSTROUTING -s $IP_LAN -o eth0 -j MASQUERADE

iptables -t nat -A PREROUTING -p tcp --dport 80 -i $I_WAN -j DNAT --to-destination $IP_DMZ_REAL_WEB:80
iptables -t nat -A PREROUTING -p tcp --dport 443 -i $I_WAN -j DNAT --to-destination $IP_DMZ_REAL_WEB:443
iptables -t nat -A PREROUTING -p udp --dport 53 -i $I_WAN -j DNAT --to-destination $IP_DMZ_REAL_DNS

#Transferencia de zona
iptables -t nat -A PREROUTING -p tcp --dport 53 -i $I_WAN -j DNAT --to-destination $IP_DMZ_REAL_DNS

#Salida
iptables -t nat -A POSTROUTING -o $I_WAN -j SNAT --to-source $IP_WAN_REAL


# Conexion established

iptables -t filter -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

iptables -t filter -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

iptables -t filter -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

