---------------------------- INICIO CONFIGURACON ENTORNO GITLAB ----------------------------

# Pequeñas instrucciones sobre como trabajar con GIT

# Primeramente tendremos que configurar nuestro nombre y email para poder correctamente los commit en GitLab o Github.
# Introducimos los siguientes comandos desde el terminal donde estemos

git config --global user.name "Pepito Amat"
git config --global user.email pepito.amat@gmail.com

# Despues debemos crear una clave privada y publica para poder comunicarnos con GIT sin utilizar usuario ni contraseña
# (se puede hacer mediante protocolo HTTPS pero si haces muchos commit diarios se hace pesado introducir los mismos datos siempre)

# Introducir este comando en el terminal

ssh-keygen -o -t rsa -b 4096 -C "pepito.amat@gmail.com"

# CLASE: En los ordenadores de clase ya que generamos un juego de claves con Sandra, tenemos que darle una vez al intro y cuando nos diga id_rsa teneis que escribir ahi id_rsa-gitlab
# para no sobreescribir el ya existente y luego ya a intro hasta salir al terminal de nuevo

# CASA: Tenemos que darle a intro, hasta que volvamos a la consola del terminal

# Con las claves generadas tenemos que darle la clave publica a GitLab para ello vamos a este enlace: https://gitlab.com/profile/keys
# Antes en el terminal ponemos cd y despues hacemos un cat id_rsa-gitlab.pub y copiamos el texto que salga debera empezar por ssh-rsa y acabar por vuestro correo
# Con la clave copiada volvemos a la web que puse arriba y pegamos la clave donde dice Key, automaticamente se rellenara el cuadro de Title, pero ahi se puede cambiar el nombre por ejemplo: Ordenador Clase - ALS

---------------------------- FIN CONFIGURACON ENTORNO GITLAB -----------------------------

---------------------------- CLONANDO UN REPOSITORO DE GITLAB ----------------------------

# CLASE: Debido a que en clase tenemos varias claves siempre en cada terminal que vayamos a utilizar comandos GIT debemos especificar la clave a gastar con este comando:

# export GIT_SSH_COMMAND='ssh -i /datos/usuarios/alumnos/a0dni/id_rsa-gitlab'

# Substitutir a0dni por el usuario de cada uno.

# Repo utilizado: https://gitlab.com/enganxat/asir-2018/dvt3/firewall
# Al entrar en la web anterior, sale una direccion que empieza por git@gitlab.com (en mi caso) en vuestro caso deberia de ser (https://gitlab.com/.....) le dais al boton que hay justo al lado de eso

# Abrimos un terminal y creamos primeramente un directorio donde alojaremos los repositorios que descargaremos o utilizaremos: mkdir repos, despues entramos en esa ruta cd repos
# Ponemos git clone y pegamos la direccion que hemos copiado arriba este seria en mi caso: git@gitlab.com:enganxat/asir-2018/dvt3/firewall.git (esta direccion no hacen falta claves) y esta (https://gitlab.com/enganxat/asir-2018/dvt3/firewall.git) si que hacen falta claves.

# git clone https://gitlab.com/enganxat/asir-2018/dvt3/firewall.git o git clone git@gitlab.com:enganxat/asir-2018/dvt3/firewall.git

# Nos dira si deseamos seguir con la autentificacion o algo parecido y debemos poner: yes o y.

# Despues de esto se clonara el repo

---------------------------- FIN CLONANDO UN REPOSITORO DE GITLAB ----------------------------

---------------------------- INICIO GIT ADD, GIT COMMIT, GIT PUSH y GIT PULL ----------------------------

# Una vez clonado nuestro repositorio vamos a hacer cambios en el, para ello vamos a crear un archivo llamado: readme-als.txt

touch readme-als.txt

# Ahora vamos a agregarlo a GIT

1º forma: suponiendo que solo hayamos cambiado eso o queramos subirlo todo de una vez

git add . 

2º forma: suponiendo que hemos modificado diversos archivos y queremos subirlo en diferentes commit.

git add readme-als.txt

# Ya esta agregado a GIT, ahora vamos a generar un commit

git commit -m "Mi primer commit - ALS"

# Ya tenemos agregado nuestro primer commit en GIT, ahora vamos a subirlo

git push

# Ya esta subido a GIT, volvemos a la web de nuestro repo, recargamos la web y deberia de verse el commit realizado

# Supongamos que estamos haciendo un trabajo y un compañero sube algo, tu no podras subir nada hasta que no haga un git pull, este comando lo que hace es sincronizar tus cambios con los suyos.
# para ello entramos en la ruta del repositorio desde el terminal

cd dvt3 # Entramos en el directorio
git init # Inicializamos el repositorio, no suele hacer falta
git pull # Sincronizamos el repositorio

Si todo ha ido bien, no deberia de dar ningun problema.

---------------------------- FIN GIT ADD, GIT COMMIT, GIT PUSH y GIT PULL ----------------------------

---------------------------- INICIO OTRAS OPERACIONES ----------------------------

-- GIT REVERT

Supongamos que un compañero realiza un cambio en un archivo y ese cambio no es correcto nosotros podemos revertirlo, para ello basta con ejecutar el siguiente comando.

Para saber el numero SHA del commit debemos ir a la pagina de los commit de nuestro repositiorios. La mia es: https://gitlab.com/enganxat/asir-2018/dvt3/firewall/commits/master
una vez en esa pagina en la parte derecha de la pantalla tenemos un numero de 8 digitos ese numero debemos de copiarlo y eso es el SHA del commit

git revert 97bae783

Una vez echo esto se abrirar una consola para darle un nombre al commit, lo mas rapido seria darle Control + X y nos saca de ahi y nos devuelve al terminal y finalmente para subirlo a git bastaria con ejecutar esto:

git push

Y ya estaria revertido el commit y subido el commmit indicandolo.

-- RAMAS EN GIT

Supongamos que hacemos un trabajo y nos repartimos el trabajo en varias personas y queremos trabajar cada uno en su parte para ello vamos a crear ramas en nuestro git utilizando el siguiente comando:

git checkout -b nueva-rama-als

Con esto estaria creada y estariamos trabajando en esa rama pero no estaria subida a GIT, para subirla a GIT se ejecuta esto:

git push

Ya estaria subida, pongamos que queremos volver a la rama principal, ponemos esto

git checkout master

Para el resto de operaciones en la rama nueva se hacen igual que si fuera normal: git add . && git commit -m "Mi primer commit - ALS" && git push

-- Copiar commit (cambios) de otro repo sin perder la autoria
-- Tanto en GitLab y en Github hay una autoridad llamada DMCA que sirve para la gente que se dedica a robar codigo sin permiso o no respeta la autoria del mismo
-- La consecuencia de la actuacion de las DMCA suele ser el cierre del repositorio afectado   y de los FORKS (os hablare mas tarde de ellos)

1º: Cuando queramos coger un commit primeramente debemos ejecutar esto

git fetch https://gitlab.com/enganxat/asir-2018/dvt3/firewall.git

(copio el enlace con HTTPS, porque se supone que en el otro no tendremos accesos con esto obtenemos los objetos que hay en ese repositiorio)

Al terminar el comando anterior, debemos ejecutar el siguiente commando para copiar el commit del compañero que tiene en su repo

git cherry-pick 97bae7834e57da0517a67ca48d19d680b10ae89f (codigo SHA del commit)

Si todo ha ido bien, nos dira que tod ha ido bien y sino nos dara un mensaje de error

Con el comando anterior copiamos los cambios y generamos el commit nos quedaria subirlo a GIT mediante el comando de siempre:

git push

---------------------------- FIN OTRAS OPERACIONES ----------------------------

Como vaya teniendo tiempo ire agregando nuevas cosas que se pueden hacer con GIT, como borrar el historico de commit sin dejar rastro, etc...